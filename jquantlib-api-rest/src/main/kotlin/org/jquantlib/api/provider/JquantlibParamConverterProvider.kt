package org.jquantlib.api.provider

import com.fasterxml.jackson.databind.ObjectMapper
import org.jquantlib.api.data.Calendar
import org.jquantlib.api.data.DayCounter
import org.jquantlib.api.data.Payoff
import java.lang.reflect.Type
import java.time.temporal.ChronoUnit
import javax.ws.rs.ext.ParamConverter
import javax.ws.rs.ext.ParamConverterProvider
import javax.ws.rs.ext.Provider

@Provider
class JquantlibParamConverterProvider(
    private val objectMapper: ObjectMapper
): ParamConverterProvider {

  private val calendarConverter: CalendarConverter by lazy { CalendarConverter() }
  private val chronoUnitConverter: ChronoUnitConverter by lazy { ChronoUnitConverter() }
  private val dayCounterConverter: DayCounterConverter by lazy { DayCounterConverter() }
  private val payoffConverter: PayoffConverter by lazy { PayoffConverter() }

  override fun <T : Any> getConverter(
      rawType: Class<T>,
      genericType: Type?,
      annotations: Array<out Annotation>?
  ) = when {
    Calendar::class.java.isAssignableFrom(rawType) -> calendarConverter
    ChronoUnit::class.java.isAssignableFrom(rawType) -> chronoUnitConverter
    DayCounter::class.java.isAssignableFrom(rawType) -> dayCounterConverter
    Payoff::class.java.isAssignableFrom(rawType) -> payoffConverter
    else -> null
  } as ParamConverter<T>?

  private inner class CalendarConverter : ParamConverter<Calendar> {
    override fun fromString(value: String): Calendar {
      try {
        return objectMapper.readerFor(Calendar::class.java).readValue(value)
      } catch (e: Exception) {
        throw IllegalArgumentException(e)
      }
    }

    override fun toString(calendar: Calendar): String {
      return objectMapper.writerFor(Calendar::class.java).writeValueAsString(calendar)
    }
  }

  private inner class DayCounterConverter : ParamConverter<DayCounter> {
    override fun fromString(value: String): DayCounter {
      try {
        return objectMapper.readerFor(DayCounter::class.java).readValue(value)
      } catch (e: Exception) {
        throw IllegalArgumentException(e)
      }
    }

    override fun toString(dayCounter: DayCounter): String {
      return objectMapper.writerFor(DayCounter::class.java).writeValueAsString(dayCounter)
    }
  }

  private inner class PayoffConverter : ParamConverter<Payoff> {
    override fun fromString(value: String): Payoff {
      try {
        return objectMapper.readerFor(Payoff::class.java).readValue(value)
      } catch (e: Exception) {
        throw IllegalArgumentException(e)
      }
    }

    override fun toString(payoff: Payoff): String {
      return objectMapper.writerFor(Payoff::class.java).writeValueAsString(payoff)
    }
  }

  private inner class ChronoUnitConverter : ParamConverter<ChronoUnit> {
    override fun fromString(value: String): ChronoUnit {
      try {
        return ChronoUnit.valueOf(value)
      } catch (e: Exception) {
        throw IllegalArgumentException(e)
      }
    }

    override fun toString(chronoUnit: ChronoUnit) = chronoUnit.name
  }

}