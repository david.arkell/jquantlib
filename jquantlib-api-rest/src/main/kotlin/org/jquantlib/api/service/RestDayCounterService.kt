package org.jquantlib.api.service

import org.jquantlib.api.data.DayCounter
import java.time.LocalDate
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.QueryParam

@Path("dayCounter")
interface RestDayCounterService {

  @GET
  @Path("dayCount")
  fun dayCount(
      @QueryParam("dayCounter") dayCounter: DayCounter,
      @QueryParam("start") start: LocalDate,
      @QueryParam("end") end: LocalDate
  ): Long

  @GET
  @Path("yearFraction")
  fun yearFraction(
      @QueryParam("dayCounter") dayCounter: DayCounter,
      @QueryParam("start") start: LocalDate,
      @QueryParam("end") end: LocalDate,
      @QueryParam("refPeriodStart") refPeriodStart: LocalDate? = null,
      @QueryParam("refPeriodEnd") refPeriodEnd: LocalDate? = null
  ): Double

}