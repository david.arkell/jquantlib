package org.jquantlib.api.service

import org.jquantlib.api.data.BusinessDayConvention
import org.jquantlib.api.data.BusinessDayConvention.Following
import org.jquantlib.api.data.Calendar
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType.APPLICATION_JSON

@Path("calendar")
interface RestCalendarService {

  @GET
  @Path("isBusinessDay")
  fun isBusinessDay(
      @QueryParam("calendar") calendar: Calendar,
      @QueryParam("date") date: LocalDate
  ): Boolean

  @GET
  @Path("isHoliday")
  fun isHoliday(
      @QueryParam("calendar") calendar: Calendar,
      @QueryParam("date") date: LocalDate
  ): Boolean

  @GET
  @Path("isWeekend")
  fun isWeekend(
      @QueryParam("calendar") calendar: Calendar,
      @QueryParam("date") date: LocalDate
  ): Boolean

  @GET
  @Path("isEndOfMonth")
  fun isEndOfMonth(
      @QueryParam("calendar") calendar: Calendar,
      @QueryParam("date") date: LocalDate
  ): Boolean

  @GET
  @Path("endOfMonth")
  @Produces(APPLICATION_JSON)
  fun endOfMonth(
      @QueryParam("calendar") calendar: Calendar,
      @QueryParam("date") date: LocalDate
  ): LocalDate

  @GET
  @Path("adjust")
  @Produces(APPLICATION_JSON)
  fun adjust(
      @QueryParam("calendar") calendar: Calendar,
      @QueryParam("date") date: LocalDate,
      @QueryParam("c") c: BusinessDayConvention = Following
  ): LocalDate

  @GET
  @Path("advance")
  @Produces(APPLICATION_JSON)
  fun advance(
      @QueryParam("calendar") calendar: Calendar,
      @QueryParam("date") date: LocalDate,
      @QueryParam("n") n: Long,
      @QueryParam("unit") unit: ChronoUnit,
      @QueryParam("c") c: BusinessDayConvention = Following,
      @QueryParam("endOfMonth") endOfMonth: Boolean = false
  ): LocalDate

  @GET
  @Path("businessDaysBetween")
  fun businessDaysBetween(
      @QueryParam("calendar") calendar: Calendar,
      @QueryParam("from") from: LocalDate,
      @QueryParam("to") to: LocalDate,
      @QueryParam("includeFirst") includeFirst: Boolean = true,
      @QueryParam("includeLast") includeLast: Boolean = false
  ): Long

}