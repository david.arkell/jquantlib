package org.jquantlib.api.service

import org.jquantlib.api.data.Payoff
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.QueryParam

@Path("payoff")
interface RestPayoffService {

  @GET
  @Path("get")
  fun get(
      @QueryParam("payoff") payoff: Payoff,
      @QueryParam("price") price: Double
  ): Double

}