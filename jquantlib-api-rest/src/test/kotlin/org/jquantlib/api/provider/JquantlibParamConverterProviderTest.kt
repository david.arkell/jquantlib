package org.jquantlib.api.provider

import org.jquantlib.api.data.*
import org.jquantlib.api.jackson.QuantlibObjectMapperFactory
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.time.temporal.ChronoUnit.*

class JquantlibParamConverterProviderTest {

  private val provider = JquantlibParamConverterProvider(QuantlibObjectMapperFactory.build())

  @Test
  fun no_converter() {
    assertNull(
        provider.getConverter(LocalDate::class.java, null, null)
    )
  }

  @Test
  fun calendar() {
    val converter = provider.getConverter(Calendar::class.java, null, null)!!

    assertEquals(
        Australia,
        converter.fromString(converter.toString(Australia))
    )
  }

  @Test(expected = IllegalArgumentException::class)
  fun calendar_fromString_fail() {
    val converter = provider.getConverter(Calendar::class.java, null, null)!!
    converter.fromString("invalid")
  }

  @Test
  fun chronoUnit() {
    val converter = provider.getConverter(ChronoUnit::class.java, null, null)!!

    assertEquals(
        DAYS,
        converter.fromString(converter.toString(DAYS))
    )
    assertEquals(
        MONTHS,
        converter.fromString(converter.toString(MONTHS))
    )
  }

  @Test(expected = IllegalArgumentException::class)
  fun chronoUnit_fromString_fail() {
    val converter = provider.getConverter(ChronoUnit::class.java, null, null)!!
    converter.fromString("invalid")
  }

  @Test
  fun dayCounter() {
    val converter = provider.getConverter(DayCounter::class.java, null, null)!!

    assertEquals(
        ActualISDA,
        converter.fromString(converter.toString(ActualISDA))
    )
  }

  @Test(expected = IllegalArgumentException::class)
  fun dayCounter_fromString_fail() {
    val converter = provider.getConverter(DayCounter::class.java, null, null)!!
    converter.fromString("invalid")
  }

  @Test
  fun payoff() {
    val converter = provider.getConverter(Payoff::class.java, null, null)!!

    val payoff = AssetOrNothingPayoff(type = OptionType.Put, strike = 13.0)

    assertEquals(
        payoff,
        converter.fromString(converter.toString(payoff))
    )
  }

  @Test(expected = IllegalArgumentException::class)
  fun payoff_fromString_fail() {
    val converter = provider.getConverter(Payoff::class.java, null, null)!!
    converter.fromString("invalid")
  }

}