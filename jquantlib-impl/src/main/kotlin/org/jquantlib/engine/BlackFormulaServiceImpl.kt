package org.jquantlib.engine

import org.apache.commons.math3.distribution.NormalDistribution
import org.jquantlib.api.data.OptionType
import org.jquantlib.api.service.BlackFormulaService
import kotlin.math.ln
import kotlin.math.max

class BlackFormulaServiceImpl: BlackFormulaService {

  private val phi = NormalDistribution()

  override fun blackFormula(
      optionType: OptionType,
      strike: Double,
      forward: Double,
      stddev: Double,
      discount: Double,
      displacement: Double
  ): Double {
    require(strike >= 0.0) { "strike must be non-negative" }
    require(forward > 0.0) { "forward must be positive" }
    require(stddev >= 0.0) { "stddev must be non-negative" }
    require(discount > 0.0) { "discount must be positive" }
    require(displacement >= 0.0) { "displacement must be non-negative" }

    val forward = forward + displacement
    val strike = strike + displacement

    if (stddev == 0.0) {
      return max((forward - strike) * optionType.value, 0.0) * discount
    }

    if (strike == 0.0) {
      // strike=0 iff displacement=0
      return if (optionType == OptionType.Call) forward * discount else 0.0
    }

    val d1 = ln(forward / strike) / stddev + 0.5 * stddev
    val d2 = d1 - stddev

    // TODO: code review
    val result = (discount * optionType.value.toDouble()
        * (forward * phi.cumulativeProbability(optionType.value * d1) - strike * phi.cumulativeProbability(optionType.value * d2)))

    if (result >= 0.0)
      return result

    throw ArithmeticException("a negative value was calculated")
  }
}