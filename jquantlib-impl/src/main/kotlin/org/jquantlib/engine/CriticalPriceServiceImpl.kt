package org.jquantlib.engine

import org.apache.commons.math3.distribution.NormalDistribution
import org.jquantlib.api.data.OptionType
import org.jquantlib.api.data.StrikedTypePayoff
import org.jquantlib.api.service.BlackFormulaService
import org.jquantlib.api.service.CriticalPriceService
import kotlin.math.abs
import kotlin.math.exp
import kotlin.math.ln
import kotlin.math.sqrt

class CriticalPriceServiceImpl(
    private val blackFormulaService: BlackFormulaService
): CriticalPriceService {

  private val cumNormalDist = NormalDistribution()

  override fun criticalPrice(
      payoff: StrikedTypePayoff,
      riskFreeDiscount: Double,
      dividendDiscount: Double,
      variance: Double,
      tolerance: Double
  ): Double {
    // Calculation of seed value, Si
    val n = 2.0 * ln(dividendDiscount / riskFreeDiscount) / (variance)
    val m = -2.0 * ln(riskFreeDiscount) / (variance)
    val bT = ln(dividendDiscount / riskFreeDiscount)

    val qu: Double
    val Su: Double
    val h: Double
    var Si: Double

    when (payoff.type) {
      OptionType.Call -> {
        qu = (-(n-1.0) + sqrt(((n-1.0)*(n-1.0)) + 4.0*m))/2.0
        Su = payoff.strike / (1.0 - 1.0/qu)
        h = -(bT + 2.0 * sqrt(variance)) * payoff.strike / (Su - payoff.strike)
        Si = payoff.strike + (Su - payoff.strike) * (1.0 - exp(h))
      }
      OptionType.Put -> {
        qu = (-(n-1.0) - sqrt(((n-1.0)*(n-1.0)) + 4.0*m))/2.0
        Su = payoff.strike / (1.0 - 1.0/qu)
        h = (bT - 2.0* sqrt(variance)) * payoff.strike / (payoff.strike - Su)
        Si = Su + (payoff.strike - Su) * exp(h)
      }
    }

    // Newton Raphson algorithm for finding critical price Si
    var Q: Double
    var LHS: Double
    var RHS: Double
    var bi: Double
    var forwardSi = Si * dividendDiscount / riskFreeDiscount
    var d1 = (ln(forwardSi/payoff.strike) + 0.5 * variance) / sqrt(variance)
    val K = if (riskFreeDiscount != 1.0) -2.0 * ln(riskFreeDiscount) / (variance * (1.0-riskFreeDiscount)) else 0.0
    val temp = blackFormulaService.blackFormula(payoff.type, payoff.strike, forwardSi, sqrt(variance)) * riskFreeDiscount

    when (payoff.type) {
      OptionType.Call -> {
        Q = (-(n-1.0) + sqrt(((n-1.0)*(n-1.0)) + 4 * K)) / 2
        LHS = Si - payoff.strike
        RHS = temp + (1 - dividendDiscount * cumNormalDist.cumulativeProbability(d1)) * Si / Q
        bi =  dividendDiscount * cumNormalDist.cumulativeProbability(d1) * (1 - 1/Q) + (1 - dividendDiscount * cumNormalDist.density(d1) / sqrt(variance)) / Q
        while (abs(LHS - RHS) / payoff.strike > tolerance) {
          Si = (payoff.strike + RHS - bi * Si) / (1 - bi)
          forwardSi = Si * dividendDiscount / riskFreeDiscount
          d1 = (ln(forwardSi/payoff.strike) + 0.5 * variance) / sqrt(variance)
          LHS = Si - payoff.strike
          val temp2 = blackFormulaService.blackFormula(payoff.type, payoff.strike, forwardSi, sqrt(variance))*riskFreeDiscount
          RHS = temp2 + (1 - dividendDiscount * cumNormalDist.cumulativeProbability(d1)) * Si / Q
          bi = dividendDiscount * cumNormalDist.cumulativeProbability(d1) * (1 - 1 / Q) + (1 - dividendDiscount * cumNormalDist.density(d1) / sqrt(variance)) / Q
        }
      }
      OptionType.Put -> {
        Q = (-(n-1.0) - sqrt(((n-1.0)*(n-1.0)) + 4 * K)) / 2
        LHS = payoff.strike - Si
        RHS = temp - (1 - dividendDiscount * cumNormalDist.cumulativeProbability(-d1)) * Si / Q
        bi = -dividendDiscount * cumNormalDist.cumulativeProbability(-d1) * (1 - 1/Q) - (1 + dividendDiscount * cumNormalDist.density(-d1) / sqrt(variance)) / Q
        while (abs(LHS - RHS) / payoff.strike > tolerance) {
          Si = (payoff.strike - RHS + bi * Si) / (1 + bi)
          forwardSi = Si * dividendDiscount / riskFreeDiscount
          d1 = (ln(forwardSi/payoff.strike) + 0.5 * variance) / sqrt(variance)
          LHS = payoff.strike - Si
          val temp2 = blackFormulaService.blackFormula(payoff.type, payoff.strike, forwardSi, sqrt(variance)) * riskFreeDiscount
          RHS = temp2 - (1 - dividendDiscount * cumNormalDist.cumulativeProbability(-d1)) * Si / Q
          bi = -dividendDiscount * cumNormalDist.cumulativeProbability(-d1) * (1 - 1 / Q) - (1 + dividendDiscount * cumNormalDist.density(-d1) / sqrt(variance)) / Q
        }
      }
    }

    return Si
  }

}