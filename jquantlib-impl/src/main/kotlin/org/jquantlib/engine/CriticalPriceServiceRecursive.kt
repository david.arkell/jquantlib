package org.jquantlib.engine

import org.apache.commons.math3.distribution.NormalDistribution
import org.jquantlib.api.data.*
import org.jquantlib.api.service.BlackFormulaService
import org.jquantlib.api.service.CriticalPriceService
import kotlin.math.abs
import kotlin.math.exp
import kotlin.math.ln
import kotlin.math.sqrt

class CriticalPriceServiceRecursive(
    private val blackFormulaService: BlackFormulaService
): CriticalPriceService {

  private val cumNormalDist = NormalDistribution()

  override fun criticalPrice(
      payoff: StrikedTypePayoff,
      riskFreeDiscount: Double,
      dividendDiscount: Double,
      variance: Double,
      tolerance: Double
  ): Double {
    // Calculation of seed value, Si
    val n = 2.0 * ln(dividendDiscount / riskFreeDiscount) / (variance)
    val m = -2.0 * ln(riskFreeDiscount) / (variance)
    val bT = ln(dividendDiscount / riskFreeDiscount)

    return when(payoff.type) {
      OptionType.Call -> criticalPriceCall(strike = payoff.strike, riskFreeDiscount = riskFreeDiscount,
          dividendDiscount = dividendDiscount, variance = variance, tolerance = tolerance,
          n = n, m = m, bT = bT)
      OptionType.Put -> criticalPricePut(strike = payoff.strike, riskFreeDiscount = riskFreeDiscount,
          dividendDiscount = dividendDiscount, variance = variance, tolerance = tolerance,
          n = n, m = m, bT = bT)
    }
  }

  private fun criticalPriceCall(
      strike: Double,
      riskFreeDiscount: Double,
      dividendDiscount: Double,
      variance: Double,
      tolerance: Double,
      n: Double,
      m: Double,
      bT: Double
  ): Double {
    val qu = (-(n-1.0) + sqrt(((n-1.0)*(n-1.0)) + 4.0*m))/2.0
    val Su = strike / (1.0 - 1.0/qu)
    val h = -(bT + 2.0 * sqrt(variance)) * strike / (Su - strike)
    val si = strike + (Su - strike) * (1.0 - exp(h))

    val forwardSi = si * dividendDiscount / riskFreeDiscount
    val d1 = (ln(forwardSi/strike) + 0.5 * variance) / sqrt(variance)
    val K = if (riskFreeDiscount != 1.0) -2.0 * ln(riskFreeDiscount) / (variance * (1.0-riskFreeDiscount)) else 0.0
    val blackFormulaByRiskFreeDiscount = blackFormulaService.blackFormula(OptionType.Call, strike, forwardSi, sqrt(variance)) * riskFreeDiscount

    val q = (-(n-1.0) + sqrt(((n-1.0)*(n-1.0)) + 4 * K)) / 2
    val lhs = si - strike
    val rhs = blackFormulaByRiskFreeDiscount + (1 - dividendDiscount * cumNormalDist.cumulativeProbability(d1)) * si / q
    val bi =  dividendDiscount * cumNormalDist.cumulativeProbability(d1) * (1 - 1/q) + (1 - dividendDiscount * cumNormalDist.density(d1) / sqrt(variance)) / q

    return criticalPriceForCallRecursive(
        lhs = lhs,
        rhs = rhs,
        strike = strike,
        tolerance = tolerance,
        si = si,
        bi = bi,
        dividendDiscount = dividendDiscount,
        riskFreeDiscount = riskFreeDiscount,
        variance = variance,
        q = q
    )
  }

  tailrec fun criticalPriceForCallRecursive(
      lhs: Double,
      rhs: Double,
      strike: Double,
      tolerance: Double,
      si: Double,
      bi: Double,
      dividendDiscount: Double,
      riskFreeDiscount: Double,
      variance: Double,
      q: Double
  ): Double {
    return if (abs(lhs - rhs) / strike > tolerance) {
      val si = (strike + rhs - bi * si) / (1 - bi)
      val forwardSi = si * dividendDiscount / riskFreeDiscount
      val d1 = (ln(forwardSi / strike) + 0.5 * variance) / sqrt(variance)
      val lhs = si - strike
      val blackFormulaByRiskFreeDiscount = blackFormulaService.blackFormula(OptionType.Call, strike, forwardSi, sqrt(variance)) * riskFreeDiscount
      val rhs = blackFormulaByRiskFreeDiscount + (1 - dividendDiscount * cumNormalDist.cumulativeProbability(d1)) * si / q
      val bi = dividendDiscount * cumNormalDist.cumulativeProbability(d1) * (1 - 1 / q) + (1 - dividendDiscount * cumNormalDist.density(d1) / sqrt(variance)) / q

      criticalPriceForCallRecursive(
          lhs = lhs,
          rhs = rhs,
          strike = strike,
          tolerance = tolerance,
          si = si,
          bi = bi,
          dividendDiscount = dividendDiscount,
          riskFreeDiscount = riskFreeDiscount,
          variance = variance,
          q = q
      )
    } else {
      si
    }
  }

  private fun criticalPricePut(
      strike: Double,
      riskFreeDiscount: Double,
      dividendDiscount: Double,
      variance: Double,
      tolerance: Double,
      n: Double,
      m: Double,
      bT: Double
  ): Double {
    val qu = (-(n-1.0) - sqrt(((n-1.0)*(n-1.0)) + 4.0*m))/2.0
    val Su = strike / (1.0 - 1.0/qu)
    val h = (bT - 2.0* sqrt(variance)) * strike / (strike - Su)
    val si = Su + (strike - Su) * exp(h)

    val forwardSi = si * dividendDiscount / riskFreeDiscount
    val d1 = (ln(forwardSi/strike) + 0.5 * variance) / sqrt(variance)
    val K = if (riskFreeDiscount != 1.0) -2.0 * ln(riskFreeDiscount) / (variance * (1.0-riskFreeDiscount)) else 0.0
    val temp = blackFormulaService.blackFormula(OptionType.Put, strike, forwardSi, sqrt(variance)) * riskFreeDiscount

    val q = (-(n-1.0) - sqrt(((n-1.0)*(n-1.0)) + 4 * K)) / 2
    val lhs = strike - si
    val rhs = temp - (1 - dividendDiscount * cumNormalDist.cumulativeProbability(-d1)) * si / q
    val bi = -dividendDiscount * cumNormalDist.cumulativeProbability(-d1) * (1 - 1/q) - (1 + dividendDiscount * cumNormalDist.density(-d1) / sqrt(variance)) / q

    return criticalPriceForPutRecursive(
        lhs = lhs,
        rhs = rhs,
        strike = strike,
        tolerance = tolerance,
        si = si,
        bi = bi,
        dividendDiscount = dividendDiscount,
        riskFreeDiscount = riskFreeDiscount,
        variance = variance,
        q = q
    )
  }

  private tailrec fun criticalPriceForPutRecursive(
      lhs: Double,
      rhs: Double,
      strike: Double,
      tolerance: Double,
      si: Double,
      bi: Double,
      dividendDiscount: Double,
      riskFreeDiscount: Double,
      variance: Double,
      q: Double
  ): Double =
      if (abs(lhs - rhs) / strike > tolerance) {
        val si = (strike - rhs + bi * si) / (1 + bi)
        val forwardSi = si * dividendDiscount / riskFreeDiscount
        val d1 = (ln(forwardSi/strike) + 0.5 * variance) / sqrt(variance)
        val lhs = strike - si
        val blackFormulaByRiskFreeDiscount = blackFormulaService.blackFormula(OptionType.Put, strike, forwardSi, sqrt(variance)) * riskFreeDiscount
        val rhs = blackFormulaByRiskFreeDiscount - (1 - dividendDiscount * cumNormalDist.cumulativeProbability(-d1)) * si / q
        val bi = -dividendDiscount * cumNormalDist.cumulativeProbability(-d1) * (1 - 1 / q) - (1 + dividendDiscount * cumNormalDist.density(-d1) / sqrt(variance)) / q

        criticalPriceForPutRecursive(
            lhs = lhs,
            rhs = rhs,
            strike = strike,
            tolerance = tolerance,
            si = si,
            bi = bi,
            dividendDiscount = dividendDiscount,
            riskFreeDiscount = riskFreeDiscount,
            variance = variance,
            q = q
        )
      } else {
        si
      }

}