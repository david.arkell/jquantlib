package org.jquantlib.engine

import org.apache.commons.math3.distribution.NormalDistribution
import org.jquantlib.api.data.*
import org.jquantlib.api.data.OptionType.*
import org.jquantlib.api.results.GreeksResults
import org.jquantlib.api.results.MoreGreeksResults
import org.jquantlib.api.results.OneAssetOptionResults
import org.jquantlib.api.service.*
import java.lang.Double.NaN
import java.time.LocalDate
import kotlin.math.*

class BaroneAdesiWhaleyApproximationEngineServiceImpl(
    private val dayCounterService: DayCounterService,
    private val yieldTermStructureService: YieldTermStructureService,
    private val blackCalculatorService: BlackCalculatorService,
    private val criticalPriceService: CriticalPriceService
) : BaroneAdesiWhaleyApproximationEngineService {

  private val PAYOFF_AT_EXPIRY_NOT_HANDLED = "payoff at expiry not handled"
  private val NON_STRIKE_PAYOFF_GIVEN = "non-striked payoff given"
  private val NEGATIVE_UNDERLYING = "negative underlying given"

  private val cumNormalDist = NormalDistribution()

  override fun calculate(
      evaluationDate: LocalDate,
      americanOption: VanillaOption<AmericanExercise>,
      bsmProcess: BlackScholesMertonProcess
  ): OneAssetOptionResults {
    val ex = americanOption.exercise
    require(!ex.payoffAtExpiry) { PAYOFF_AT_EXPIRY_NOT_HANDLED }
    require(americanOption.payoff is StrikedTypePayoff) { NON_STRIKE_PAYOFF_GIVEN }
    val payoff = americanOption.payoff as StrikedTypePayoff

    val lastDate = americanOption.exercise.lastDate()
    val variance = blackVariance(bsmProcess, lastDate)
    val dividendDiscount = yieldTermStructureService.discount(bsmProcess.dividendTS, lastDate)
    val riskFreeDiscount = yieldTermStructureService.discount(bsmProcess.riskFreeTS, lastDate)
    val spot = bsmProcess.x0.value
    require(spot > 0.0) { NEGATIVE_UNDERLYING }

    val bc = blackCalculator(bsmProcess, lastDate, payoff, spot)

    return if (dividendDiscount >= 1.0 && payoff.type == Call) {
      // early exercise never optimal
      val t = dayCounterService.yearFraction(bsmProcess.blackVolTS.dayCounter, bsmProcess.blackVolTS.referenceDate, lastDate)

      OneAssetOptionResults(
          value = blackCalculatorService.value(bc),
          errorEstimate = NaN,
          greeks = GreeksResults(
              delta = blackCalculatorService.delta(bc, spot),
              gamma = blackCalculatorService.gamma(bc, spot),
              theta = blackCalculatorService.theta(bc, spot, t),
              vega = blackCalculatorService.vega(bc, t),
              rho = blackCalculatorService.rho(
                  bc,
                  dayCounterService.yearFraction(bsmProcess.riskFreeTS.dayCounter, bsmProcess.riskFreeTS.referenceDate, lastDate)
              ),
              dividendRho = blackCalculatorService.dividendRho(
                  bc,
                  dayCounterService.yearFraction(bsmProcess.dividendTS.dayCounter, bsmProcess.dividendTS.referenceDate, lastDate)
              )
          ),
          moreGreeks = MoreGreeksResults(
              itmCashProbability = blackCalculatorService.itmCashProbability(bc),
              deltaForward = blackCalculatorService.deltaForward(bc),
              elasticity = blackCalculatorService.elasticity(bc, spot),
              thetaPerDay = blackCalculatorService.thetaPerDay(bc, spot, t),
              strikeSensitivity = blackCalculatorService.strikeSensitivity(bc)
          )
      )
    } else {
      // early exercise can be optimal
      val tolerance = 1e-6
      val Sk: Double = criticalPriceService.criticalPrice(payoff, riskFreeDiscount, dividendDiscount, variance, tolerance)
      val forwardSk = Sk * dividendDiscount / riskFreeDiscount
      val d1 = (ln(forwardSk / payoff.strike) + 0.5 * variance) / sqrt(variance)
      val n = 2.0 * ln(dividendDiscount / riskFreeDiscount) / variance
      val K = -2.0 * ln(riskFreeDiscount) / (variance * (1.0 - riskFreeDiscount))

      val value = when (payoff.type) {
        Call -> {
          val Q = (-(n - 1.0) + sqrt((n - 1.0) * (n - 1.0) + 4.0 * K)) / 2.0
          val a = Sk / Q * (1.0 - dividendDiscount * cumNormalDist.cumulativeProbability(d1))
          if (spot < Sk)
            blackCalculatorService.value(bc) + a * (spot / Sk).pow(Q)
          else
            spot - payoff.strike
        }
        Put -> {
          val Q = (-(n - 1.0) - sqrt((n - 1.0) * (n - 1.0) + 4.0 * K)) / 2.0
          val a = -(Sk / Q) * (1.0 - dividendDiscount * cumNormalDist.cumulativeProbability(-d1))
          if (spot > Sk)
            blackCalculatorService.value(bc) + a * (spot / Sk).pow(Q)
          else
            payoff.strike - spot
        }
      }

      OneAssetOptionResults(
          value = value,
          errorEstimate = NaN,
          greeks = GreeksResults(
              delta = NaN,
              gamma = NaN,
              theta = NaN,
              vega = NaN,
              rho = NaN,
              dividendRho = NaN
          ),
          moreGreeks = MoreGreeksResults(
              itmCashProbability = NaN,
              deltaForward = NaN,
              elasticity = NaN,
              thetaPerDay = NaN,
              strikeSensitivity = NaN
          )
      )
    } // end of "early exercise can be optimal"
  }

  private fun blackCalculator(
      bsmProcess: BlackScholesMertonProcess,
      lastDate: LocalDate,
      payoff: StrikedTypePayoff,
      spot: Double
  ): BlackCalculator {
    val variance = blackVariance(bsmProcess, lastDate)
    val dividendDiscount = yieldTermStructureService.discount(bsmProcess.dividendTS, lastDate)
    val riskFreeDiscount = yieldTermStructureService.discount(bsmProcess.riskFreeTS, lastDate)

    val forwardPrice = spot * dividendDiscount / riskFreeDiscount

    return blackCalculatorService.create(
        payoff = payoff,
        forward = forwardPrice,
        stdDev = sqrt(variance),
        discount = riskFreeDiscount
    )
  }

  private fun blackVariance(
      bsmProcess: BlackScholesMertonProcess,
      maturity: LocalDate
  ): Double {
    val time = dayCounterService.yearFraction(
        dayCounter = bsmProcess.blackVolTS.dayCounter,
        start = bsmProcess.blackVolTS.referenceDate,
        end = maturity
    )

    return blackVariance(
        bsmProcess = bsmProcess,
        maturity = time
    )
  }

  private fun blackVariance(
      bsmProcess: BlackScholesMertonProcess,
      maturity: Double
  ): Double {
    val volatility = bsmProcess.blackVolTS.volatility.value

    return volatility * volatility * maturity
  }

}