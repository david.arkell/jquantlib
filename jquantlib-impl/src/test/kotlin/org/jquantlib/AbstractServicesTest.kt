/*
 Copyright (C) 2019 David Arkell

 This source code is release under the BSD License.

 This file is part of JQuantLib, a free-software/open-source library
 for financial quantitative analysts and developers - http://jquantlib.org/

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the license for more details.

 JQuantLib is based on QuantLib. http://quantlib.org/
 When applicable, the original copyright notice follows this notice.
 */

package org.jquantlib

import org.jquantlib.calculator.BlackCalculatorServiceImpl
import org.jquantlib.calendar.CalendarServiceImpl
import org.jquantlib.dayCounter.DayCounterServiceImpl
import org.jquantlib.ir.InterestRateServiceImpl
import org.jquantlib.payoff.PayoffServiceImpl
import org.jquantlib.yts.YieldTermStructureServiceImpl

abstract class AbstractServicesTest {

  val blackCalculatorService = BlackCalculatorServiceImpl()

  val payoffService =  PayoffServiceImpl()

  val calendarService = CalendarServiceImpl()

  val dayCounterService = DayCounterServiceImpl(
      calendarService = calendarService
  )

  val interestRateService = InterestRateServiceImpl(
      dayCounterService = dayCounterService
  )

  val yieldTermStructureService = YieldTermStructureServiceImpl(
      calendarService = calendarService,
      dayCounterService = dayCounterService,
      interestRateService = interestRateService
  )

}