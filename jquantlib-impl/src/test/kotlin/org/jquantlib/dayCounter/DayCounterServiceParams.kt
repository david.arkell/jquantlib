/*
 Copyright (C) 2019 David Arkell

 This source code is release under the BSD License.

 This file is part of JQuantLib, a free-software/open-source library
 for financial quantitative analysts and developers - http://jquantlib.org/

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the license for more details.

 JQuantLib is based on QuantLib. http://quantlib.org/
 When applicable, the original copyright notice follows this notice.
 */

package org.jquantlib.dayCounter

import com.fasterxml.jackson.core.type.TypeReference
import org.jquantlib.ExpectedAsAny
import org.jquantlib.ExpectedAsDouble
import org.jquantlib.api.data.DayCounter
import java.time.LocalDate

object ListDayCountTypeReference : TypeReference<List<DayCountParams>>()

data class DayCountParams(
    val dayCounter: DayCounter,
    val start: LocalDate,
    val end: LocalDate,
    override val expected: Long
): ExpectedAsAny<Long>

object ListYearFractionParamsTypeReference : TypeReference<List<YearFractionParams>>()

data class YearFractionParams(
    val dayCounter: DayCounter,
    val start: LocalDate,
    val end: LocalDate,
    override val expected: Double
): ExpectedAsDouble

