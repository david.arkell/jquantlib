/*
 Copyright (C) 2019 David Arkell

 This source code is release under the BSD License.

 This file is part of JQuantLib, a free-software/open-source library
 for financial quantitative analysts and developers - http://jquantlib.org/

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the license for more details.

 JQuantLib is based on QuantLib. http://quantlib.org/
 When applicable, the original copyright notice follows this notice.
 */

package org.jquantlib.dayCounter

import org.jquantlib.AbstractServicesTest
import org.jquantlib.DataLoader.assertEqualsAny
import org.jquantlib.DataLoader.assertEqualsDouble
import org.junit.Test

class DayCounterServiceTest: AbstractServicesTest() {

  @Test
  fun dayCount() {
    assertEqualsAny("DayCounter_dayCount.json", ListDayCountTypeReference) {
      dayCounterService.dayCount(it.dayCounter, it.start, it.end)
    }
  }

  @Test
  fun yearFraction() {
    assertEqualsDouble("DayCounter_yearFraction.json", ListYearFractionParamsTypeReference) {
      dayCounterService.yearFraction(it.dayCounter, it.start, it.end)
    }
  }

}