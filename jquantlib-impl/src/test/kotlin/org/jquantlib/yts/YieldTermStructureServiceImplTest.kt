/*
 Copyright (C) 2019 David Arkell

 This source code is release under the BSD License.

 This file is part of JQuantLib, a free-software/open-source library
 for financial quantitative analysts and developers - http://jquantlib.org/

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the license for more details.

 JQuantLib is based on QuantLib. http://quantlib.org/
 When applicable, the original copyright notice follows this notice.
 */

package org.jquantlib.yts

import org.jquantlib.AbstractServicesTest
import org.jquantlib.api.data.*
import org.jquantlib.api.data.Compounding.*
import org.jquantlib.api.data.Frequency.Once
import org.junit.Test
import java.time.LocalDate

class YieldTermStructureServiceImplTest: AbstractServicesTest() {

  @Test(expected = IllegalArgumentException::class)
  fun discount_fail_with_invalid_frequency() {
    yieldTermStructureService.discount(
        yts = FlatForward(
            referenceDate = LocalDate.of(2019, 1, 15),
            dayCounter = Actual365Fixed,
            forward = SimpleQuote(
                value = 30.0
            ),
            compounding = Simple,
            frequency = Once
        ),
        d = LocalDate.of(2019, 1, 15)
    )
  }

}