/*
 Copyright (C) 2019 David Arkell

 This source code is release under the BSD License.

 This file is part of JQuantLib, a free-software/open-source library
 for financial quantitative analysts and developers - http://jquantlib.org/

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the license for more details.

 JQuantLib is based on QuantLib. http://quantlib.org/
 When applicable, the original copyright notice follows this notice.
 */

package org.jquantlib.yts

import com.fasterxml.jackson.core.type.TypeReference
import org.jquantlib.AbstractServicesTest
import org.jquantlib.DataLoader.dataForEach
import org.jquantlib.api.data.*
import org.junit.Assert.assertEquals
import org.junit.Ignore
import org.junit.Test
import java.time.LocalDate

class FlatForwardDiscountTest: AbstractServicesTest() {

  @Test
  @Ignore
  fun discount_date() {
    dataForEach("/FlatForward_discount_date.json", ListParams1TypeReference) {
      assertEquals(
          it.toString(),
          it.expected,
          yieldTermStructureService.discount(it.flatForward, it.date),
          1e-10
      )
    }
  }

  @Test
  @Ignore
  fun discount_time() {
    dataForEach("/FlatForward_discount_time.json", ListParams2TypeReference) {
      assertEquals(
          it.toString(),
          it.expected,
          yieldTermStructureService.discount(it.flatForward, it.time),
          1e-10
      )
    }
  }

  object ListParams1TypeReference : TypeReference<List<Params1>>()

  data class Params1(
      val flatForward: FlatForward,
      val date: LocalDate,
      val expected: Double
  )

  object ListParams2TypeReference : TypeReference<List<Params2>>()

  data class Params2(
      val flatForward: FlatForward,
      val time: Double,
      val expected: Double
  )

}
