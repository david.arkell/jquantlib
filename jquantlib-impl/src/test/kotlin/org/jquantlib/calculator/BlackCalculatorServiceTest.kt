/*
 Copyright (C) 2019 David Arkell

 This source code is release under the BSD License.

 This file is part of JQuantLib, a free-software/open-source library
 for financial quantitative analysts and developers - http://jquantlib.org/

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the license for more details.

 JQuantLib is based on QuantLib. http://quantlib.org/
 When applicable, the original copyright notice follows this notice.
 */

package org.jquantlib.calculator

import org.jquantlib.AbstractServicesTest
import org.jquantlib.api.data.OptionType.Put
import org.jquantlib.api.data.PlainVanillaPayoff
import org.junit.Ignore
import org.junit.Test

class BlackCalculatorServiceTest: AbstractServicesTest() {

  @Test(expected = IllegalArgumentException::class)
  fun create_with_invalid_forward() {
    blackCalculatorService.create(
        payoff = PlainVanillaPayoff(
            type = Put,
            strike = 0.0
        ),
        forward = -1.0,
        stdDev = 1.0,
        discount = 1.0
    )
  }

  @Test(expected = IllegalArgumentException::class)
  fun create_with_invalid_stdDev() {
    blackCalculatorService.create(
        payoff = PlainVanillaPayoff(
            type = Put,
            strike = 0.0
        ),
        forward = 1.0,
        stdDev = -1.0,
        discount = 1.0
    )
  }

  @Test(expected = IllegalArgumentException::class)
  fun create_with_invalid_discount() {
    blackCalculatorService.create(
        payoff = PlainVanillaPayoff(
            type = Put,
            strike = 0.0
        ),
        forward = 1.0,
        stdDev = 1.0,
        discount = -1.0
    )
  }

  private fun validBc() = blackCalculatorService.create(
      payoff = PlainVanillaPayoff(
          type = Put,
          strike = 0.0
      ),
      forward = 1.0,
      stdDev = 1.0,
      discount = 1.0
  )

  @Test(expected = IllegalArgumentException::class)
  fun delta_with_invalid_spot() {
    blackCalculatorService.delta(
        bc = validBc(),
        spot = -1.0
    )
  }

  @Test(expected = IllegalArgumentException::class)
  fun gamma_with_invalid_spot() {
    blackCalculatorService.gamma(
        bc = validBc(),
        spot = -1.0
    )
  }

  @Test(expected = IllegalArgumentException::class)
  fun theta_with_invalid_maturity() {
    blackCalculatorService.theta(
        bc = validBc(),
        spot = 1.0,
        maturity = -1.0
    )
  }

  @Test(expected = IllegalArgumentException::class)
  fun vega_with_invalid_maturity() {
    blackCalculatorService.vega(
        bc = validBc(),
        maturity = -1.0
    )
  }

  @Test(expected = IllegalArgumentException::class)
  fun rho_with_invalid_maturity() {
    blackCalculatorService.rho(
        bc = validBc(),
        maturity = -1.0
    )
  }

  @Test(expected = IllegalArgumentException::class)
  fun dividendRho_with_invalid_maturity() {
    blackCalculatorService.dividendRho(
        bc = validBc(),
        maturity = -1.0
    )
  }

  @Test
  fun value() {
    assertEqualsBlackCalculatorParamsExpected("BlackCalculator_value.json") {
      blackCalculatorService.value(it.bc())
    }
  }

  @Test
  fun delta() {
    assertEqualsBlackCalculatorSpotParamsExpected("BlackCalculator_delta.json") {
      blackCalculatorService.delta(it.bc(), it.spot)
    }
  }

  @Test
  fun deltaForward() {
    assertEqualsBlackCalculatorParamsExpected("BlackCalculator_deltaForward.json") {
      blackCalculatorService.deltaForward(it.bc())
    }
  }

  @Test
  fun elasticity() {
    assertEqualsBlackCalculatorSpotParamsExpected("BlackCalculator_elasticity.json") {
      blackCalculatorService.elasticity(it.bc(), it.spot)
    }
  }

  @Test
  fun elasticityForward() {
    assertEqualsBlackCalculatorParamsExpected("BlackCalculator_elasticityForward.json") {
      blackCalculatorService.elasticityForward(it.bc())
    }
  }

  @Test
  fun gamma() {
    assertEqualsBlackCalculatorSpotParamsExpected("BlackCalculator_gamma.json") {
      blackCalculatorService.gamma(it.bc(), it.spot)
    }
  }

  @Test
  fun gammaForward() {
    assertEqualsBlackCalculatorParamsExpected("BlackCalculator_gammaForward.json") {
      blackCalculatorService.gammaForward(it.bc())
    }
  }

  @Test
  fun theta() {
    assertEqualsBlackCalculatorSpotMaturityParamsExpected("BlackCalculator_theta.json") {
      blackCalculatorService.theta(it.bc(), it.spot, it.maturity)
    }
  }

  @Test
  @Ignore
  fun thetaPerDay() {
    assertEqualsBlackCalculatorSpotMaturityParamsExpected("BlackCalculator_thetaPerDay.json") {
      blackCalculatorService.thetaPerDay(it.bc(), it.spot, it.maturity)
    }
  }

  @Test
  fun vega() {
    assertEqualsBlackCalculatorSpotParamsExpected("BlackCalculator_vega.json") {
      blackCalculatorService.vega(it.bc(), it.spot)
    }
  }

  @Test
  fun rho() {
    assertEqualsBlackCalculatorMaturityParamsExpected("BlackCalculator_rho.json") {
      blackCalculatorService.rho(it.bc(), it.maturity)
    }
  }

  @Test
  fun dividendRho() {
    assertEqualsBlackCalculatorMaturityParamsExpected("BlackCalculator_dividendRho.json") {
      blackCalculatorService.dividendRho(it.bc(), it.maturity)
    }
  }

  @Test
  fun itmCashProbability() {
    assertEqualsBlackCalculatorParamsExpected("BlackCalculator_itmCashProbability.json") {
      blackCalculatorService.itmCashProbability(it.bc())
    }
  }

  @Test
  fun itmAssetProbability() {
    assertEqualsBlackCalculatorParamsExpected("BlackCalculator_itmAssetProbability.json") {
      blackCalculatorService.itmAssetProbability(it.bc())
    }
  }

  @Test
  fun strikeSensitivity() {
    assertEqualsBlackCalculatorParamsExpected("BlackCalculator_strikeSensitivity.json") {
      blackCalculatorService.strikeSensitivity(it.bc())
    }
  }

  private fun BlackCalculatorParams.bc() =
      blackCalculatorService.create(
          payoff = strikedTypePayoff,
          forward = forward,
          stdDev = stdDev,
          discount = discount
      )

}