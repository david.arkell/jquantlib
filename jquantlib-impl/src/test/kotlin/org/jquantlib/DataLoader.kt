/*
 Copyright (C) 2019 David Arkell

 This source code is release under the BSD License.

 This file is part of JQuantLib, a free-software/open-source library
 for financial quantitative analysts and developers - http://jquantlib.org/

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the license for more details.

 JQuantLib is based on QuantLib. http://quantlib.org/
 When applicable, the original copyright notice follows this notice.
 */

package org.jquantlib

import com.fasterxml.jackson.core.type.TypeReference
import org.jquantlib.api.jackson.QuantlibObjectMapperFactory
import org.junit.Assert.assertEquals
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.security.InvalidParameterException
import java.util.zip.GZIPInputStream

object DataLoader {

  private val mapper = QuantlibObjectMapperFactory.build()

  private const val error = 1e-10

  fun <T> data(fileName: String, typeReference: TypeReference<T>): T {
    return mapper
        .readerFor(typeReference)
        .readValue<T>(decompress(DataLoader::class.java.getResourceAsStream("/${fileName}.gzip")))
  }

  private fun decompress(inputStream: InputStream): ByteArray {
    try {
      val gis = GZIPInputStream(inputStream)
      val baos = ByteArrayOutputStream()

      gis.copyTo(baos)

      baos.close()
      gis.close()

      return baos.toByteArray()
    } catch (e: IOException) {
      throw InvalidParameterException()
    }
  }

  fun <T> dataForEach(filename: String, tr: TypeReference<List<T>>, f: (T) -> Unit) {
    data(filename, tr).forEach { f(it) }
  }

  fun <T: ExpectedAsDouble> assertEqualsDouble(filename: String, tr: TypeReference<List<T>>, f: (T) -> Double) {
    data(filename, tr).forEach {
      assertEquals(
          it.toString(),
          it.expected,
          f(it),
          error
      )
    }
  }

  fun <T: ExpectedAsAny<*>> assertEqualsAny(filename: String, tr: TypeReference<List<T>>, f: (T) -> Any) {
    data(filename, tr).forEach {
      assertEquals(
          it.toString(),
          it.expected,
          f(it)
      )
    }
  }

}

interface ExpectedAsDouble {
  val expected: Double
}

interface ExpectedAsAny<T> {
  val expected: T
}