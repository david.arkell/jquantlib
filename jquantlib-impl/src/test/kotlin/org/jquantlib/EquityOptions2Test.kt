package org.jquantlib

import org.jquantlib.api.data.OptionType
import org.jquantlib.calculator.BlackCalculatorServiceImpl
import org.jquantlib.calendar.CalendarServiceImpl
import org.jquantlib.dayCounter.DayCounterServiceImpl
import org.jquantlib.daycounters.Actual365Fixed
import org.jquantlib.engine.BaroneAdesiWhaleyApproximationEngineServiceImpl
import org.jquantlib.engine.BlackFormulaServiceImpl
import org.jquantlib.engine.CriticalPriceServiceImpl
import org.jquantlib.exercise.AmericanExercise
import org.jquantlib.instruments.Option.Type.Put
import org.jquantlib.instruments.Option.Type.Call
import org.jquantlib.instruments.PlainVanillaPayoff
import org.jquantlib.instruments.VanillaOption
import org.jquantlib.ir.InterestRateServiceImpl
import org.jquantlib.pricingengines.vanilla.BaroneAdesiWhaleyApproximationEngine
import org.jquantlib.processes.BlackScholesMertonProcess
import org.jquantlib.quotes.Handle
import org.jquantlib.quotes.Quote
import org.jquantlib.quotes.SimpleQuote
import org.jquantlib.termstructures.BlackVolTermStructure
import org.jquantlib.termstructures.YieldTermStructure
import org.jquantlib.termstructures.volatilities.BlackConstantVol
import org.jquantlib.termstructures.yieldcurves.FlatForward
import org.jquantlib.time.Date
import org.jquantlib.time.Month.May
import org.jquantlib.time.calendars.Target
import org.jquantlib.yts.YieldTermStructureServiceImpl
import org.junit.Test
import java.io.*
import java.security.InvalidParameterException
import java.time.LocalDate
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

class EquityOptions2Test {

//  @Test
//  fun gzip() {
//    val p = "/Users/darkell/Documents/IdeaProjects/jquantlib/jquantlib-impl/src/test/resources"
//
//    File(p)
//        .listFiles { _, name -> name.endsWith(".gzip") }
//        .forEach {
//          println(String(decompressGzipFile(it)).subSequence(0, 100))
////          gzipFile(it, File(it.parent, "${it.name}.gzip"))
//        }
//  }

//  private fun decompressGzipFile(gzipFile: File): ByteArray {
//    try {
//      val fis = FileInputStream(gzipFile)
//      val gis = GZIPInputStream(fis)
//      val baos = ByteArrayOutputStream()
//
//      gis.copyTo(baos)
//
//      baos.close()
//      gis.close()
//
//      return baos.toByteArray()
//    } catch (e: IOException) {
//      e.printStackTrace()
//      throw InvalidParameterException()
//    }
//  }

//  private fun gzipFile(file: File, gzipFile: File) {
//    try {
//      val fis = FileInputStream(file)
//      val fos = FileOutputStream(gzipFile)
//      val gzipOS = GZIPOutputStream(fos)
//
//      fis.copyTo(gzipOS)
//
//      gzipOS.close()
//      fos.close()
//      fis.close()
//    } catch (e: IOException) {
//      e.printStackTrace()
//    }
//  }

  @Test
  fun test() {
    // set up dates
    val calendar = Target()
    val todaysDate = Date(15, May, 1998)
    val settlementDate = Date(17, May, 1998)
    Settings().setEvaluationDate(todaysDate)

    // our options
    val type = Put
    val strike = 40.0
    val underlying = 36.0
    val riskFreeRate = 0.06
    val volatility = 0.2
    val dividendYield = 0.00

    val maturity = Date(17, May, 1999)
    val dayCounter = Actual365Fixed()

    // Define exercise for European Options
    // val europeanExercise = EuropeanExercise(maturity)

    // bootstrap the yield/dividend/volatility curves
    val underlyingH = Handle<Quote>(SimpleQuote(underlying))
    val flatDividendTS = Handle<YieldTermStructure>(FlatForward(settlementDate, dividendYield, dayCounter))
    val flatTermStructure = Handle<YieldTermStructure>(FlatForward(settlementDate, riskFreeRate, dayCounter))
    val flatVolTS = Handle<BlackVolTermStructure>(BlackConstantVol(settlementDate, calendar, volatility, dayCounter))
    val payoff = PlainVanillaPayoff(type, strike)

    val bsmProcess = BlackScholesMertonProcess(underlyingH, flatDividendTS, flatTermStructure, flatVolTS)

    val americanExercise = AmericanExercise(settlementDate, maturity)
    val americanOption = VanillaOption(payoff, americanExercise)

    americanOption.setPricingEngine(BaroneAdesiWhaleyApproximationEngine(bsmProcess))
    val npv = americanOption.NPV()

    println("npv = $npv")
    // errorEstimate not provided
    // println("errorEstimate = ${americanOption.errorEstimate()}")
    println("delta = ${americanOption.delta()}")
    println("gamma = ${americanOption.gamma()}")
    println("theta = ${americanOption.theta()}")
    println("vega = ${americanOption.vega()}")
    println("rho = ${americanOption.rho()}")
    println("dividendRho = ${americanOption.dividendRho()}")

    println("itmCashProbability = ${americanOption.itmCashProbability()}")
    println("deltaForward = ${americanOption.deltaForward()}")
    println("elasticity = ${americanOption.elasticity()}")
    println("thetaPerDay = ${americanOption.thetaPerDay()}")
    println("strikeSensitivity = ${americanOption.strikeSensitivity()}")
  }

  @Test
  fun test_new() {

    val calendarService = CalendarServiceImpl()
    val dayCounterService = DayCounterServiceImpl(
        calendarService = calendarService
    )
    val interestRateService = InterestRateServiceImpl(
        dayCounterService = dayCounterService
    )
    val yieldTermStructureService = YieldTermStructureServiceImpl(
        calendarService = calendarService,
        dayCounterService = dayCounterService,
        interestRateService = interestRateService
    )
    val blackCalculatorService = BlackCalculatorServiceImpl()
    val baroneAdesiWhaleyApproximationEngineService = BaroneAdesiWhaleyApproximationEngineServiceImpl(
        dayCounterService = dayCounterService,
        yieldTermStructureService = yieldTermStructureService,
        blackCalculatorService = blackCalculatorService,
        criticalPriceService = CriticalPriceServiceImpl(BlackFormulaServiceImpl())
    )

    val evaluationDate = LocalDate.of(1998, 5, 15)
    val settlementDate = LocalDate.of(1998, 5, 17)
    val type = OptionType.Call
    val strike = 40.0
    val underlying = 36.0
    val riskFreeRate = 0.06
    val volatility = 0.2
    val dividendYield = 0.00

    val maturity = LocalDate.of(1999, 5, 17)
    val dayCounter = org.jquantlib.api.data.Actual365Fixed

    val underlyingH = org.jquantlib.api.data.SimpleQuote(underlying)

    val flatDividendTS = org.jquantlib.api.data.FlatForward(
        referenceDate = settlementDate,
        dayCounter = dayCounter,
        forward = org.jquantlib.api.data.SimpleQuote(
            value = dividendYield
        )
    )
    val flatTermStructure = org.jquantlib.api.data.FlatForward(
        referenceDate = settlementDate,
        dayCounter = dayCounter,
        forward = org.jquantlib.api.data.SimpleQuote(
            value = riskFreeRate
        )
    )
    val flatVolTS = org.jquantlib.api.data.BlackConstantVol(
        referenceDate = settlementDate,
        volatility = org.jquantlib.api.data.SimpleQuote(
            value = volatility
        ),
        calendar = org.jquantlib.api.data.Target,
        dayCounter = dayCounter
    )
    val payoff = org.jquantlib.api.data.PlainVanillaPayoff(
        type = type,
        strike = strike
    )
    val bsmProcess = org.jquantlib.api.data.BlackScholesMertonProcess(
        x0 = underlyingH,
        dividendTS = flatDividendTS,
        riskFreeTS = flatTermStructure,
        blackVolTS = flatVolTS
    )
    val americanExercise = org.jquantlib.api.data.AmericanExercise(
        earliestDate = settlementDate,
        latestDate = maturity
    )
    val americanOption = org.jquantlib.api.data.VanillaOption<org.jquantlib.api.data.AmericanExercise>(
        payoff = payoff,
        exercise = americanExercise
    )

    val results = baroneAdesiWhaleyApproximationEngineService.calculate(
        evaluationDate = evaluationDate,
        americanOption = americanOption,
        bsmProcess = bsmProcess
    )

    println("npv = $results")
  }

}