package org.jquantlib

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider
import org.apache.cxf.ext.logging.LoggingFeature
import org.apache.cxf.jaxrs.provider.JavaTimeTypesParamConverterProvider
import org.apache.cxf.jaxrs.spring.SpringComponentScanServer
import org.jquantlib.api.jackson.QuantlibObjectMapperFactory
import org.jquantlib.api.provider.JquantlibParamConverterProvider
import org.jquantlib.api.service.CalendarService
import org.jquantlib.api.service.DayCounterService
import org.jquantlib.api.service.PayoffService
import org.jquantlib.calendar.CalendarServiceImpl
import org.jquantlib.dayCounter.DayCounterServiceImpl
import org.jquantlib.payoff.PayoffServiceImpl
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@SpringBootApplication
@Import(SpringComponentScanServer::class)
@Configuration
class App {

  @Bean
  fun objectMapper() = QuantlibObjectMapperFactory.build()

  @Bean
  fun jacksonJsonProvider(objectMapper: ObjectMapper) = JacksonJsonProvider(objectMapper)

  @Bean
  fun loggingFeature() = LoggingFeature()

  @Bean
  fun javaTimeTypesParamConverterProvider() = JavaTimeTypesParamConverterProvider()

  @Bean
  fun jquantlibParamConverterProvider(objectMapper: ObjectMapper) = JquantlibParamConverterProvider(objectMapper)

  @Bean
  fun calendarService(): CalendarService = CalendarServiceImpl()

  @Bean
  fun dayCounterService(calendarService: CalendarService): DayCounterService = DayCounterServiceImpl(calendarService)

  @Bean
  fun payoffService(): PayoffService = PayoffServiceImpl()

}

fun main(args: Array<String>) {
  runApplication<App>(*args)
}
