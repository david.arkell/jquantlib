package org.jquantlib.services

import org.jquantlib.api.data.DayCounter
import org.jquantlib.api.service.DayCounterService
import org.jquantlib.api.service.RestDayCounterService
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
@Component
class RestDayCounterServiceImpl(
    private val dayCounterService: DayCounterService
): RestDayCounterService {

  override fun dayCount(dayCounter: DayCounter, start: LocalDate, end: LocalDate): Long {
    return dayCounterService.dayCount(dayCounter, start, end)
  }

  override fun yearFraction(dayCounter: DayCounter, start: LocalDate, end: LocalDate, refPeriodStart: LocalDate?, refPeriodEnd: LocalDate?): Double {
    return dayCounterService.yearFraction(dayCounter, start, end, refPeriodStart, refPeriodEnd)
  }

}