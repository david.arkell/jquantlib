package org.jquantlib.services

import org.jquantlib.api.data.Payoff
import org.jquantlib.api.service.PayoffService
import org.jquantlib.api.service.RestPayoffService
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

@Service
@Component
class RestPayoffServiceImpl(
    private val payoffService: PayoffService
): RestPayoffService {

  override fun get(payoff: Payoff, price: Double): Double {
    return payoffService.get(payoff, price)
  }

}