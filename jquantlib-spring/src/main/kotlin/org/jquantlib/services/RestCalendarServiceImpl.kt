package org.jquantlib.services

import org.jquantlib.api.data.BusinessDayConvention
import org.jquantlib.api.data.Calendar
import org.jquantlib.api.service.CalendarService
import org.jquantlib.api.service.RestCalendarService
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.temporal.ChronoUnit

@Service
@Component
class RestCalendarServiceImpl(
    private val calendarService: CalendarService
): RestCalendarService {

  override fun isBusinessDay(calendar: Calendar, date: LocalDate): Boolean {
    return calendarService.isBusinessDay(calendar, date)
  }

  override fun isHoliday(calendar: Calendar, date: LocalDate): Boolean {
    return calendarService.isHoliday(calendar, date)
  }

  override fun isWeekend(calendar: Calendar, date: LocalDate): Boolean {
    return calendarService.isWeekend(calendar, date)
  }

  override fun isEndOfMonth(calendar: Calendar, date: LocalDate): Boolean {
    return calendarService.isEndOfMonth(calendar, date)
  }

  override fun endOfMonth(calendar: Calendar, date: LocalDate): LocalDate {
    return calendarService.endOfMonth(calendar, date)
  }

  override fun adjust(calendar: Calendar, date: LocalDate, c: BusinessDayConvention): LocalDate {
    return calendarService.adjust(calendar, date, c)
  }

  override fun advance(calendar: Calendar, date: LocalDate, n: Long, unit: ChronoUnit, c: BusinessDayConvention, endOfMonth: Boolean): LocalDate {
    return calendarService.advance(calendar, date, n, unit, c, endOfMonth)
  }

  override fun businessDaysBetween(calendar: Calendar, from: LocalDate, to: LocalDate, includeFirst: Boolean, includeLast: Boolean): Long {
    return calendarService.businessDaysBetween(calendar, from, to, includeFirst, includeLast)
  }

}