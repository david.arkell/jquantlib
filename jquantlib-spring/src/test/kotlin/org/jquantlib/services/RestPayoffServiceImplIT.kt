package org.jquantlib.services

import org.jquantlib.api.data.OptionType
import org.jquantlib.api.data.PlainVanillaPayoff
import org.junit.Assert.assertEquals
import org.junit.Test

class RestPayoffServiceImplIT: AbstractRestService() {

  @Test
  fun get() {
    assertEquals(
        23.0,
        restPayoffService.get(
            payoff = PlainVanillaPayoff(
                type = OptionType.Put,
                strike = 35.0
            ),
            price = 12.0
        ),
        1e-10
    )
  }

}