package org.jquantlib.services

import org.jquantlib.api.data.SimpleDayCounter
import org.junit.Assert.assertEquals
import org.junit.Test
import java.time.LocalDate

class RestDayCounterServiceImplIT: AbstractRestService() {

  @Test
  fun dayCount() {
    assertEquals(
        30L,
        restDayCounterService.dayCount(
            SimpleDayCounter,
            LocalDate.of(2019, 5, 26),
            LocalDate.of(2019, 6, 26)
        )
    )
  }

  @Test
  fun yearFraction() {
    assertEquals(
        0.08333333333333333,
        restDayCounterService.yearFraction(
            SimpleDayCounter,
            LocalDate.of(2019, 5, 26),
            LocalDate.of(2019, 6, 26)
        ),
        1e-10
    )
  }

}