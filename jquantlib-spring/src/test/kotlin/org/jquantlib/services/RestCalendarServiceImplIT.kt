package org.jquantlib.services

import org.jquantlib.api.data.Australia
import org.jquantlib.api.data.BusinessDayConvention.Following
import org.junit.Assert.*
import org.junit.Test
import java.time.LocalDate
import java.time.temporal.ChronoUnit.DAYS
import java.time.temporal.ChronoUnit.MONTHS

class RestCalendarServiceImplIT: AbstractRestService() {

  @Test
  fun isBusinessDay() {
    assertTrue(restCalendarService.isBusinessDay(Australia, LocalDate.of(2019, 6, 26)))
  }

  @Test
  fun isHoliday() {
    assertFalse(restCalendarService.isHoliday(Australia, LocalDate.of(2019, 6, 26)))
  }

  @Test
  fun isWeekend() {
    assertFalse(restCalendarService.isWeekend(Australia, LocalDate.of(2019, 6, 26)))
    assertTrue(restCalendarService.isWeekend(Australia, LocalDate.of(2019, 6, 29)))
  }

  @Test
  fun isEndOfMonth() {
    assertTrue(restCalendarService.isEndOfMonth(Australia, LocalDate.of(2019, 6, 30)))
  }

  @Test
  fun endOfMonth() {
    assertEquals(
        LocalDate.of(2019, 6, 28),
        restCalendarService.endOfMonth(Australia, LocalDate.of(2019, 6, 26))
    )
  }

  @Test
  fun adjust() {
    assertEquals(
        LocalDate.of(2019, 7, 1),
        restCalendarService.adjust(Australia, LocalDate.of(2019, 6, 29), Following)
    )
  }

  @Test
  fun advance() {
    assertEquals(
        LocalDate.of(2019, 6, 20),
        restCalendarService.advance(
            Australia,
            LocalDate.of(2019, 6, 13),
            5L,
            DAYS,
            Following,
            false
        )
    )

    assertEquals(
        LocalDate.of(2019, 11, 13),
        restCalendarService.advance(
            Australia,
            LocalDate.of(2019, 6, 13),
            5L,
            MONTHS,
            Following,
            false
        )
    )
  }

  @Test
  fun businessDaysBetween() {
    assertEquals(
        22L,
        restCalendarService.businessDaysBetween(
            Australia,
            LocalDate.of(2019, 6, 13),
            LocalDate.of(2019, 7, 13),
            true,
            false
        )
    )
  }

}