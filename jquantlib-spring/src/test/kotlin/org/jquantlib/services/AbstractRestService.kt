package org.jquantlib.services

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider
import org.apache.cxf.ext.logging.LoggingFeature
import org.apache.cxf.jaxrs.client.JAXRSClientFactory
import org.apache.cxf.jaxrs.provider.JavaTimeTypesParamConverterProvider
import org.jquantlib.api.jackson.QuantlibObjectMapperFactory
import org.jquantlib.api.provider.JquantlibParamConverterProvider
import org.jquantlib.api.service.RestCalendarService
import org.jquantlib.api.service.RestDayCounterService
import org.jquantlib.api.service.RestPayoffService
import kotlin.reflect.KClass

abstract class AbstractRestService {

  private val url = "http://localhost:8080/services"

  val restCalendarService = createServiceClient(RestCalendarService::class)

  val restDayCounterService = createServiceClient(RestDayCounterService::class)

  val restPayoffService = createServiceClient(RestPayoffService::class)

  private fun <T : Any> createServiceClient(clazz: KClass<T>): T {
    return JAXRSClientFactory.create(
        url,
        clazz.java,
        listOf(
            JacksonJsonProvider(QuantlibObjectMapperFactory.build()),
            JavaTimeTypesParamConverterProvider(),
            JquantlibParamConverterProvider(QuantlibObjectMapperFactory.build())
        ),
        listOf(
            LoggingFeature()
        ),
        null)
  }

}