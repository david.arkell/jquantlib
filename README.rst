This code base is based on this https://github.com/frgomes/jquantlib

It was developed with the reasonable rational of staying close to the original C++ as possible which is reasonable.

However, I've decided to go in a different direction as listed below:
 * The chosen language is Kotlin instead of Java, because I like Kotlin better.
 * The logic has been moved out of the data objects and into services.
 * The original code was written to run on a desktop, where my expertise lies in creating back end services.
 * Also this means that the objects that are used can be immutable, which I generally think is preferable. It might cause problems down the line but I'll deal with that if or when it occurs.
