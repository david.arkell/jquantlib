package org.jquantlib.api.service

import org.jquantlib.api.data.OptionType

interface BlackFormulaService {

  fun blackFormula(
      optionType: OptionType,
      strike: Double,
      forward: Double,
      stddev: Double,
      discount: Double = 1.0,
      displacement: Double = 0.0
  ): Double
}