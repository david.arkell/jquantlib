package org.jquantlib.api.service

import org.jquantlib.api.data.StrikedTypePayoff

interface CriticalPriceService {

  fun criticalPrice(
      payoff: StrikedTypePayoff,
      riskFreeDiscount: Double, /*@DiscountFactor*/
      dividendDiscount: Double, /*@DiscountFactor*/
      variance: Double,
      tolerance: Double
  ): Double

}