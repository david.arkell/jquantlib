package org.jquantlib.api.service

import org.jquantlib.api.data.AmericanExercise
import org.jquantlib.api.data.BlackScholesMertonProcess
import org.jquantlib.api.data.VanillaOption
import org.jquantlib.api.results.OneAssetOptionResults
import java.time.LocalDate

interface BaroneAdesiWhaleyApproximationEngineService {

  fun calculate(
      evaluationDate: LocalDate,
      americanOption: VanillaOption<AmericanExercise>,
      bsmProcess: BlackScholesMertonProcess
  ): OneAssetOptionResults

}